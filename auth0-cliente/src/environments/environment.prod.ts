import {clientId, domain} from "../../auth0_config.json";

export const environment = {
  production: true,
  auth0: {
    domain,
    clientId,
    redirectUri: 'http://localhost:4200'
  }
};
