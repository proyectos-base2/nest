import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RutaUsuarioComponent } from './ruta-usuario/ruta-usuario.component';
import { RutaInicioComponent } from './ruta-inicio/ruta-inicio.component';
import { RutaTresComponent } from './ruta-tres/ruta-tres.component';
import {AuthModule} from "@auth0/auth0-angular";
import {environment} from "../environments/environment";

@NgModule({
  declarations: [
    AppComponent,
    RutaUsuarioComponent,
    RutaInicioComponent,
    RutaTresComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AuthModule.forRoot({
      ...environment.auth0
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
