import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaInicioComponent} from "./ruta-inicio/ruta-inicio.component";
import {RutaUsuarioComponent} from "./ruta-usuario/ruta-usuario.component";
import {RutaTresComponent} from "./ruta-tres/ruta-tres.component";
import {AuthGuard} from "@auth0/auth0-angular";

const routes: Routes = [
  {
    path: 'inicio',
    component: RutaInicioComponent
  },
  {
    path:'usuarios',
    component: RutaUsuarioComponent,
    canActivate: [AuthGuard]
  },
  {
    path:'',
    redirectTo: 'inicio',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: RutaTresComponent,
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
