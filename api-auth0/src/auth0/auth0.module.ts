import { Module } from '@nestjs/common';
import  * as dotenv from 'dotenv';
import {PassportModule} from "@nestjs/passport";
import {Auth0Strategy} from "./auth0.strategy";
dotenv.config();

@Module({
    imports: [
        PassportModule
            .register({defaultStrategy: 'jwt'})
    ],
    providers: [Auth0Strategy],
    exports: [PassportModule]
})
export class Auth0Module {
    constructor() {
        console.log('env', process.env.VARIABLE_NOMBRE)
    }
}
