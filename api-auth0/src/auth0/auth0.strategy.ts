import  * as dotenv from 'dotenv';
import {Injectable} from "@nestjs/common";
import {PassportStrategy} from "@nestjs/passport";
import {ExtractJwt,Strategy} from "passport-jwt"
import {passportJwtSecret} from "jwks-rsa";

dotenv.config();

@Injectable()
export class Auth0Strategy extends PassportStrategy(Strategy) {
    constructor() {
        super(
            {
                secretOrKeyProvider: passportJwtSecret({
                    cache: true,
                    rateLimit: true,
                    jwksRequestsPerMinute: 5,
                    jwksUri: `${process.env.AUTH0_DOMINIO}.well-known/jwks.json`
                }),
                jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
                audience: process.env.AUTH0_AUDIENCE,
                issuer: `${process.env.AUTH0_DOMINIO}`,
                algorithms: ['RS256']
            }
        );
        console.log(' process.env.AUTH0_AUDIENCE', process.env.AUTH0_AUDIENCE)
        console.log(' process.env.AUTH0_DOMINIO', process.env.AUTH0_DOMINIO)
    }

    validate(payload:any){
        return payload;
    }
}