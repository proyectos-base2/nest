import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import {Reflector} from "@nestjs/core";

@Injectable()
export class PermissionsGuard implements CanActivate {

  constructor(private readonly reflector: Reflector) {
  }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {

    const permisosRuta = this.reflector.get<string[]>
    ('permissions', context.getHandler())

    const permisosUsuario = context.getArgs()[0].user.permissions;
    // permisosUsuario.push('listar:usuarios')
    if(!permisosRuta){
      return true;
    }

    const tienePermisos = ()=>
        permisosRuta.every(
            rutaPermiso => permisosUsuario.includes(rutaPermiso)
        )
    return tienePermisos();

  }
}
