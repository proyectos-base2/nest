import {
    Controller,
    Get,
    Delete,
    Post,
    Put,
    Param,
    Body,
    NotFoundException,
    BadRequestException,
    UseGuards
} from '@nestjs/common';
import {UsuarioService} from "./usuario.service";
import {AuthGuard} from "@nestjs/passport";
import {PermissionsGuard} from "../permissions.guard";
import {Permissions} from "../permissions.decorator";


@Controller('usuario')
export class UsuarioController {

    constructor(private readonly usuarioService: UsuarioService) {
    }


    // @Get('/obtener-usuario')
    @UseGuards(AuthGuard('jwt'), PermissionsGuard)
    @Get('/lista-usuarios')
    @Permissions('listar:usuarios')
    getUsuarios(){
        const usuarios = this.usuarioService.listarUsuarios()
        return {
            mensaje: "lista de usuarios",
            datos: usuarios
        }
    }
// http://localhost:3000/usuario/eliminar/6
// http://localhost:3000/usuario/eliminar/23
    @Delete('/eliminar/:id')
    eliminarUsuario(@Param('id') idUsuario){
        console.log('id en eliminar', idUsuario)
        const usuarioEliminado = this.usuarioService.eliminarPorId(idUsuario)
       if(usuarioEliminado) {
           return {
               mensaje: 'usuario eliminado',
               usuario : usuarioEliminado

           }
       }else {
           throw new BadRequestException()
       }

    }

    // http://localhost:3000/usuario/23
    @Get(':id')
    getUsuarioPorId(@Param('id') idUsuario){
        const usuarioEncontrado = this.usuarioService.buscarPorId(idUsuario)
        if(usuarioEncontrado.length > 0){
            return {
                mensaje: "Busqueda por id",
                usuario: usuarioEncontrado
            }
        }else {
            throw new NotFoundException();
        }

    }

    @Post('crear')
    crearUsuario(@Body() datosUsuario){
        console.log('datos post', datosUsuario)
        const usuarioCreado = this.usuarioService.crearUsuario(datosUsuario)
        return {
            mensaje: 'usuario creado',
            datos: usuarioCreado
        }
    }

    @Put('actualizar/:id')
    actualizarUsuario(@Body() datosUsuario,
                      @Param('id') id){
        const usuarioActualizado = this.usuarioService.actualizar(id, datosUsuario)
        if(usuarioActualizado){
            return {
                mensaje: 'usuario actualizado',
                datos: datosUsuario,
                idUsuario: id
            }
        }else {
            throw new BadRequestException()
        }

    }
    
}
