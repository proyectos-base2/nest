import { Injectable } from '@nestjs/common';
import {JwtService} from "@nestjs/jwt";

@Injectable()
export class AutenticacionService {

    constructor(private readonly jwtTokenService: JwtService) {
    }

    loginConCredecianles(usuario){
        const payload = {
            nombreUsuario: usuario.usuario,
            passwordUsuario: usuario.password
        }
        // retornar el accesst token codificado
        return this.jwtTokenService.sign(payload)
    }
}
