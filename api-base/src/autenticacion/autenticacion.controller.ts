import { Controller, Post, Body } from '@nestjs/common';
import {AutenticacionService} from "./autenticacion.service";

@Controller('autenticacion')
export class AutenticacionController {

    constructor(private readonly autenticacionService: AutenticacionService) {
    }
    @Post('/login')
    login(@Body() credenciales: CredencialesInterface){
        const jwt = this.autenticacionService.loginConCredecianles(credenciales)
        return {
            access_token: jwt
        }
    }
}

interface CredencialesInterface {
    usuario:string;
    password: string;
}
