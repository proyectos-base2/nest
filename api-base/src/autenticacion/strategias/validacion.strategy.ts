import {PassportStrategy} from "@nestjs/passport";
import {Strategy} from "passport-local";
import {UsuarioService} from "../../usuario/usuario.service";
import {Injectable, UnauthorizedException} from "@nestjs/common";

@Injectable()
export class ValidacionStrategy extends PassportStrategy(Strategy) {
    constructor(
        private readonly userService: UsuarioService
    ) {
        super();
    }

    async validate(usuario: string, password:string) {
        const listaUsuarios = this.userService.listarUsuarios();
        const usuarioEncontrado = listaUsuarios.filter(
            (usuario: any) =>
                usuario.name === usuario && usuario.password === password)
        if(usuarioEncontrado.length > 0) {
            return usuarioEncontrado[0]
        }else {
            throw new UnauthorizedException()
        }
    }
}